qpid-proton (0.37.0-2) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Support multiple Python versions (Closes: #1024300).

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Nov 2022 11:36:59 +0100

qpid-proton (0.37.0-1) unstable; urgency=medium

  * New upstream release.
  * Adopt the package in the OpenStack team, as there's no other maintainers
    than me anymore.
  * Restart git history using upstream Git repo as base.
  * Use openstack-pkg-tools.
  * Removed all old patches.
  * Removed all traces of proton-c: files have moved upstream.
  * Fix python3-qpid-proton.install.
  * rm debian/README.source.
  * Fix 3x *doc.install files.
  * rm d/libqpid-proton11.docs.
  * Add continue-building-with-deprecated-declarations.patch. This fixes FTBFS
    with OpenSSL 3.0 (Closes: #1006562).
  * Added Restrictions: superficial to tests (Closes: #974503).
  * Removed python2 autopkgtest.
  * autopkgtest: only test with default python3, not with all available
    python3.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Feb 2022 10:04:14 +0100

qpid-proton (0.22.0-5) unstable; urgency=medium

  * Re-upload source-only.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 13:55:17 +0200

qpid-proton (0.22.0-4) unstable; urgency=medium

  * Remove Daniel Pocock and Darryl L. Pierce from uploaders.
  * Ran wrap-and-sort -bastk.
  * Switched to debhelper-compat =10 instead of d/compat.
  * ACK previous uploads:
    - Drop Python 2 support and stop building the doc with epydoc, which is now
      removed from Debian.
    - Remove epydoc build-depends.
  * Reintroduce python-qpid-proton-doc package, this time using sphinx.

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Mar 2020 12:03:23 +0100

qpid-proton (0.22.0-3.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #938313

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jan 2020 22:49:44 -0500

qpid-proton (0.22.0-3.2) unstable; urgency=medium

  * Replace previous NMU with a new source-only upload.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 08 Aug 2019 02:29:46 +0000

qpid-proton (0.22.0-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Eliminate dependency on epydoc, which will be removed (closes: #881561)
    - Update debian/control to remove Build-Depends: python-epydoc
    - Update debian/control to remove now-empty python-qpid-proton-doc package
    - Remove debian/python-qpid-proton-doc.install

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 02 Aug 2019 17:54:46 +0000

qpid-proton (0.22.0-3) unstable; urgency=medium

  * Using https for debian/watch.
  * List Barclays and others in debian/copyright (Closes: #896117).
  * Remove examples/c/include from d/copyright (gone from current upstream
    sources).
  * Using https in d/copyright Source, and d/control Homepage fields.
  * Various other fixes for d/copyright.
  * Add breaks+replaces for old versions, and fix doc symlinks as per advice
    from Adrian Bunk (Closes: #896507).
  * Fix build issue with GCC by applying upstream patch (Closes: #897841).
  * Point VCS URLs to Salsa.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Jan 2019 17:16:42 +0100

qpid-proton (0.22.0-2) unstable; urgency=high

  * Fix i386 build issue by using DEB_BUILD_GNU_TYPE instead of
    DEB_TARGET_MULTIARCH.

 -- Daniel Pocock <daniel@pocock.pro>  Fri, 20 Apr 2018 10:00:34 +0100

qpid-proton (0.22.0-1) unstable; urgency=high

  * New upstream release.
  * Built against OpenSSL 1.1.  (Closes: #859669)
  * Patch to avoid mismatch with hardening flags.  (Closes: #871087)
  * Rebuilt with GCC 7.  (Closes: #871292)

 -- Daniel Pocock <daniel@pocock.pro>  Tue, 17 Apr 2018 10:39:22 +0200

qpid-proton (0.14.0-5) unstable; urgency=medium

  * Team upload.
  * Change libqpid-proton-cpp8-dev depend to libqpid-proton-cpp8
    (Closes: #857411)

 -- Ondřej Nový <onovy@debian.org>  Fri, 10 Mar 2017 22:56:38 +0100

qpid-proton (0.14.0-4) unstable; urgency=medium

  * Team upload.
  * Removed C++ symbols file - it's really pain to maintain it

 -- Ondřej Nový <onovy@debian.org>  Tue, 22 Nov 2016 00:13:40 +0100

qpid-proton (0.14.0-3) unstable; urgency=medium

  * Team upload.

  [ Daniel Pocock ]
  * Replace legacy packages. (Closes: #843024)

  [ Ondřej Nový ]
  * Use OpenSSL 1.0, because OpenSSL 1.1 is not supported (Closes: #828521)
  * Bumped debhelper compat version to 10
  * d/control:
    - Use https protocol for Vcs-Git
    - Minor fixies in short descriptions
  * d/copyright: Fixed copyright for moved file
  * Added symbols file
  * Added simple autopkgtests for Python bindings

 -- Daniel Pocock <daniel@pocock.pro>  Thu, 03 Nov 2016 10:24:19 +0100

qpid-proton (0.14.0-2) unstable; urgency=medium

  * Tidy up lintian warnings.

 -- Daniel Pocock <daniel@pocock.pro>  Sat, 24 Sep 2016 18:50:36 +0200

qpid-proton (0.14.0-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Pocock <daniel@pocock.pro>  Thu, 22 Sep 2016 10:21:16 +0200

qpid-proton (0.10-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 09:18:57 +0000

qpid-proton (0.10-1) experimental; urgency=medium

  * Added myself as uploader.
  * New upstream release (Closes: #806794):
    - Fixed *-examples.install so that it pickups the folder in
      usr/share/proton-0.10/examples and not usr/share/proton/examples.
  * Switch build-depends from python (>= version) and python-dev (>= version)
    to python-all and python-dev-all.
  * Ran wrap-and-sort -t -a.
  * Added Python3 support (Closes: #806795).

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Dec 2015 14:00:53 +0000

qpid-proton (0.7-2) unstable; urgency=low

  * Change dependency from libsslcommon2-dev to libssl-dev. (Closes: #768617)
  * Bumped Standards-Version to 3.9.6.
  * Added support for dh_python2.

 -- Darryl L. Pierce <dpierce@redhat.com>  Wed, 12 Nov 2014 15:58:45 -0500

qpid-proton (0.7-1) unstable; urgency=low

  * Initial release. (Closes: #732302)

 -- Daniel Pocock <daniel@pocock.pro>  Mon, 02 Jun 2014 22:05:01 +0200
